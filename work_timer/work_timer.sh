#!/bin/bash

worktime=45
breaktime=$(($worktime/3))
session=0

while [[ TRUE ]] ; do
	while [[ $session < $worktime ]] ; do
		sleep 15m
		session=$(($session+15))
		echo $session
		notify-send -u 'critical' "It's been ${session} minutes" -t 100
	done
	mplayer -really-quiet alert.mp3
	notify-send -u 'critical' "Break Time" -t 100
	sleep ${breaktime}m
	feh break_over.png
	session=0
done
