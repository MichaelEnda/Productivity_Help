required packages:

mplayer

Set worktime for increments of 15 minutes. For each increment of work time you get 33% break time.

The script should be completely silent. If mplayer is outputting an error run it without the -realy-quiet option and check what the error is
if the error has to do with lirc try adding "lirc=no" to ~/.mplayer/config. If that doesn't work I can't help you, sorry.
