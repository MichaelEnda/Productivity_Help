# #!/media/Windows/Users/micha/Documents/Productivity_Help/homeworktimer/venv/bin/python
import time
import os
import csv
import datetime
from threading import Thread

# Use the threading example from here https://stackoverflow.com/questions/22180915/non-polling-non-blocking-timer


classlist = ['Phil 3600: Philosophy of Religion', 'Phil 3700: Political Philosophy',
             'Phil 4110: Ancient Greek Philosophy', 'Phil 5700: Adv Poltical Philosophy',
             'Math 5510: Topology', 'Math 4420: Complex Analysis']
print classlist
course_input = raw_input('What homework are you working on? (choose number 1 - %s): ' % (len(classlist)))
course = int(course_input) - 1
if (raw_input('You selected %s, is this correct? (Y,N):' % (classlist[course]) ) == 'N'):
    course_input = raw_input('What homework are you working on? (choose number 1 - %s): ' % (len(classlist)))

timertype = raw_input('How would you like to count the time(c - continuous, b-block): ')


if timertype == 'b':
    workperiod = input('How long (minutes) do you want to work for?: ')
    worktime = 0 #minutes
    #look up threading in python
    try:
        while True:
            '''keytest = khbit.KBHit()
        if keytest.kbhit() == True:
                same = input('Are you still working on %s (1/0) :' % (course))
                if same == 0:
                    input_data = (datetime.date.today(), course, worktime)
                    with open('homework_data', 'a') as data:
                        writer = csv.writer(data)
                        writer.writerow(input_data)
                    course = raw_input('What are you wokring on now?: ')
                    worktime = 0'''
            if worktime % 15 == 0 & worktime != 0:
                print 'You have been working on %s for %s minutes.' % (classlist[course], worktime)
                os.system('notify-send -u critical "You have been working for %s minutes"' % (worktime) )
            if worktime % workperiod == 0 and worktime != 0:
                os.system("notify-send -u critical 'Break Time'")
                #raw_input('Acknowledge break Time')
                breaktime = input('How long (minutes) do you want your break to be?: ')
                time.sleep(60*breaktime)
                os.system("notify-send -u critical 'Back to work'")
                os.system("feh break_over.png")
                same = input('Are you still working on %s (1/0) :' % (classlist[course]))
                if same == 1:
                    workperiod = input('How long (minutes) do you want to work for?: ')
                    worktime = 0
                if same == 0:
                    input_data = (datetime.date.today(), classlist[course], worktime)
                    with open('homework_data', 'a') as data:
                        writer = csv.writer(data)
                        writer.writerow(input_data)
                    print 'Log has been updated'
                    print classlist
                    course_input = raw_input('What homework are you working on? (choose number 1 - %s): ' % (len(classlist)))
                    course = int(course_input) - 1
                    worktime = 0
                    workperiod = input('How long (minutes) do you want to work for?: ')
            worktime += 1
            time.sleep(60)
    except KeyboardInterrupt:
        pass


if timertype == 'c':
    print 'Starting to count, good luck.'
    while True:
        worktime = 0
        time.sleep(60)
        worktime += 1
        if worktime % 10 == 0:
            os.system('notify-send -u critical "You have been working for %s minutes"' % (worktime))